import { useState } from "react";

export default function LoginForm(props: { loading: boolean, submit: any }) {
    const [formData, setFormData] = useState({ email: '', password: '' });

    const handleFormChange = (e: any) => {
        setFormData({ ...formData, [e.target.name]: e.target.value});
    }

    return (
        <form onSubmit={(e) => props.submit(e, formData)}>
            <div>
                <label htmlFor="email">Email</label>
                <input id="email" type="email" name="email" onChange={handleFormChange}></input>
            </div>
            <div>
                <label htmlFor="password">Password</label>
                <input id="password" type="password" name="password" onChange={handleFormChange}></input>
            </div>
            <button type="submit" disabled={props.loading}>
                {props.loading ? 'Loading...' : 'Submit'}
            </button>
        </form>
    );
}