export default function LoginSuccess(props: {user: any}) {
    return (
        <div>
            {`Logged in as ${props.user.email}`}
        </div>
    )
}