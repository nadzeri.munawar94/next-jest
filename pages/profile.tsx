import { useEffect, useState } from "react"

export default function Profile() {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [user, setUser] = useState({} as {avatar: string, first_name: string, last_name: string, email: string});

    useEffect(() => {
        setLoading(true);
        fetch('https://reqres.in/api/users/1')
            .then((response) => response.json())
            .then((data) => {
                setLoading(false);
                setUser(data.data);
            })
            .catch((error) => {
                setLoading(false);
                setError(error.message);
            });
    }, [])

    return (
        <div className="containe">
            <h1 className="title">My Profile</h1>
            {loading ? <div className="content">Loading...</div> : ''}
            {error ? <div className="content">There is error, the error message: { error }</div> : ''}
            {user && !error && !loading ? <div>
                <img className="avatar" src={user.avatar} />
                <div className="name">Name: {user.first_name + ' ' + user.last_name}</div>
                <div className="email">Email: {user.email}</div>
            </div> : '' }
        </div>
    )
}