import { useState } from "react"
import LoginForm from "../modules/Login/LoginForm";
import LoginSuccess from "../modules/Login/LoginSuccess";

export default function Login() {
    const [loginState, setLoginState] = useState('ANONYMOUS');
    const [loading, setLoading] = useState(false);
    const [user, setUser] = useState(null as any);

    const onSuccess = (user) => {
        setLoginState('LOGGED_IN');
        setUser(user);
        setLoading(false);
    }

    const submit = (e: any, formData: any) => {
        e.preventDefault();

        setLoading(true);
        fetch('www.binar.com/users', { method: 'POST', body: JSON.stringify(formData) })
            .then((response) => response.json())
            .then(onSuccess)
    }

    return (
        <section>
            { loginState === 'ANONYMOUS' ? <LoginForm loading={loading} submit={submit} /> : '' }
            { loginState === 'LOGGED_IN' ? <LoginSuccess user={user} /> : '' }
        </section>
    )
}