import { useState } from "react";

export default function Counter() {
    const [counter, setCounter] = useState('0');

    return (
        <div className="container">
            <div className="counter-text">{counter}</div>
            <button className="counter-button" onClick={() => {
                if (counter === '9+') {
                 return;    
                } 
                
                if (Number(counter) >= 9) {
                    setCounter('9+')
                } else {
                    setCounter(String(Number(counter) + 1))
                }
                
            }}>Count</button>
        </div>
    );
}