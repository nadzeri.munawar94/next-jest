import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import Counter from '../pages/counter';

describe('Counter', () => {
    it('Counter text initially display 0', () => {
        const { container } = render(<Counter />);
        const counterText = container.querySelector('.counter-text')?.textContent;
        expect(counterText).toEqual('0');
    });

    it('Counter text should be 1 if click counter button once', async() => {
        const { container } = render(<Counter />);

        await userEvent.click(screen.getByText('Count'))

        const counterText = container.querySelector('.counter-text')?.textContent;
        expect(counterText).toEqual('1');
    });

    it('Counter text should be 5 if click counter button clicked 5 times', async() => {
        const { container } = render(<Counter />);

        await userEvent.click(screen.getByText('Count'))
        await userEvent.click(screen.getByText('Count'))
        await userEvent.click(screen.getByText('Count'))
        await userEvent.click(screen.getByText('Count'))
        await userEvent.click(screen.getByText('Count'))

        const counterText = container.querySelector('.counter-text')?.textContent;
        expect(counterText).toEqual('5');
    });

    it('Counter text should be 9+ if click counter button clicked 10 times', async() => {
        const { container } = render(<Counter />);

        for (let i = 0; i < 10; i++) {
            await userEvent.click(screen.getByText('Count'))
        }

        const counterText = container.querySelector('.counter-text')?.textContent;
        expect(counterText).toEqual('9+');
    });

    it('Counter text should be 9+ if click counter button clicked 10 times', async() => {
        const { container } = render(<Counter />);

        for (let i = 0; i < 20; i++) {
            await userEvent.click(screen.getByText('Count'))
        }

        const counterText = container.querySelector('.counter-text')?.textContent;
        expect(counterText).toEqual('9+');
    });
});