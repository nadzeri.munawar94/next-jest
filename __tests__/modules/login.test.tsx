import Login from "@/pages/login";
import { render, screen, fireEvent, act } from "@testing-library/react";
import React from "react";

describe("LoginModule", () => {
    fit('successful login flow', async () => {
        global.fetch = jest.fn().mockResolvedValue({
            json: jest.fn().mockResolvedValue({ token: '123' })
        });

        render(<Login />);
        
        const emailField = screen.getByLabelText('Email');
        const passwordField = screen.getByLabelText('Password');
        const button = screen.getByRole('button');

        // fill out and submit form
        fireEvent.change(emailField, { target: { value: 'test@email.com' } });
        fireEvent.change(passwordField, { target: { value: 'password' } });
        fireEvent.click(button);

        await act(() => {
            expect(global.fetch).toHaveBeenCalled();
        });

        expect(button).toBeDisabled();
        expect(button).toHaveTextContent('Loading...');
    })
})