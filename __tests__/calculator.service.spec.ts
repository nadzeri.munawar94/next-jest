import { CalculatorService } from "../services/calculator.service";

describe("CalculatorService", () => {
  let service: CalculatorService;

  beforeEach(() => {
    service = new CalculatorService();
  });

  describe("#sum", () => {
    it("1 + 1 should return 2", () => {
      expect(service.sum(1, 1)).toEqual(2);
    });

    it("15 + 15 should return 30", () => {
      expect(service.sum(15, 15)).toEqual(30);
    });

    it("-150 + 15 should return -135", () => {
      expect(service.sum(-150, 15)).toEqual(-135);
    });
  });

  describe("#divide", () => {
    it("10 / 5 should return 2", () => {
      expect(service.divide(10, 5)).toEqual(2);
    });

    it("100 / 4 should return 25", () => {
      expect(service.divide(100, 4)).toEqual(25);
    });

    it("10 / 4 should return 2.5", () => {
      expect(service.divide(10, 4)).toEqual(2.5);
    });

    it("10 / 0 should return Infinity", () => {
      expect(service.divide(10, 0)).toEqual(Infinity);
    });

    it("-10 / 100 should return -0.1", () => {
      expect(service.divide(-10, 100)).toEqual(-0.1);
    });
  });
});
