import { render, screen, waitFor } from '@testing-library/react'
import React from 'react';
import Profile from '../pages/profile';

describe('Profile', () => {
    it('Title "My Profile" should be displayed', () => {
        global.fetch = jest.fn().mockResolvedValue({
            json: jest.fn().mockResolvedValue('')
        });
        
        jest.spyOn(React, 'useState').mockReturnValue(['', () => {}])
        
        const { container } = render(<Profile />);
        const titleText = container.querySelector('.title')?.textContent;
        expect(titleText).toEqual('My Profile');
    })

    it('Content "Loading..." should be displayed when loading', () => {
        global.fetch = jest.fn().mockResolvedValue({
            json: jest.fn().mockResolvedValue('')
        });
        jest.spyOn(React, 'useState')
            .mockReturnValueOnce([true, () => {}])
            .mockReturnValue(['', () => {}]);

        const { container } = render(<Profile />);

        const contentText = container.querySelector('.content')?.textContent;
        expect(contentText).toEqual('Loading...');
    })

    it('Content "There is error, the error message:" should be displayed when error', async() => {
        global.fetch = jest.fn().mockResolvedValue({
            json: jest.fn().mockRejectedValue({
                message: 'Ini error message'
            })
        });
        jest.spyOn(React, 'useState')
            .mockReturnValueOnce([false, () => {}])
            .mockReturnValueOnce(['Ini error message', () => {}])
            .mockReturnValueOnce([{}, () => {}]);

        const { container } = render(<Profile />);
        const contentText = container.querySelector('.content')?.textContent;
        expect(contentText).toContain('There is error, the error message:');
    })

    it('Avatar image, name, and email should exist when data has been fetched', () => {
        global.fetch = jest.fn().mockResolvedValue({
            json: jest.fn().mockResolvedValue('')
        });
        jest.spyOn(React, 'useState')
            .mockReturnValueOnce([false, () => {}])
            .mockReturnValueOnce([null, () => {}])
            .mockReturnValueOnce([{
                data: {
                    email: 'email',
                    first_name: 'f_name',
                    last_name: 'l_name',
                    avatar: 'avatar_url'
                }
            }, () => {}]);
        
        const { container } = render(<Profile />);

        const avatarElement = container.querySelector('.avatar');
        expect(avatarElement).not.toBeNull();

        const nameElement = container.querySelector('.name');
        expect(nameElement?.textContent).toContain('Name:');

        const emailElement = container.querySelector('.email');
        expect(emailElement?.textContent).toContain('Email:');
    })
});