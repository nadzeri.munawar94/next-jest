export class CalculatorService {
  sum(a: number, b: number) {
    return a + b;
  }

  divide(a: number, b: number) {
    return a / b;
  }
}
